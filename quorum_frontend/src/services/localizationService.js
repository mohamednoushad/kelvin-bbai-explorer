import en from '../i18n/en';
import ch from '../i18n/ch';

const languages = {
	en,
	ch
};

let defaultLanguage = 'ch';

window.myData = languages[defaultLanguage];

window.changeLanguage = lang => {
	window.myData = languages[lang];
};

export function my8n(key) {
	return window.myData[key];
}
