import { my8n } from '../services/localizationService';

function formatTime(timestamp) {
	var currentTime = new Date().getTime();
	timestamp = timestamp * 1000;
	// timestamp = Number(timestamp * 10 ** -6);
	var seconds = Math.floor((currentTime - timestamp) / 1000);
	var minutes = Math.floor(seconds / 60);
	var hours = Math.floor(minutes / 60);
	var days = Math.floor(hours / 24);
	var returnValue = '';

	let daysInLanguage = my8n('days');

	returnValue += days > 0 ? days + daysInLanguage : '';
	hours = hours - days * 24;

	let hoursInLanguage = my8n('hours');

	returnValue +=
		hours > 0 ? (returnValue !== '' ? ' ' : '') + hours + hoursInLanguage : '';
	if (returnValue.indexOf('days') === -1) {
		minutes = minutes - days * 24 * 60 - hours * 60;

		let minsInLanguage = my8n('mins');

		returnValue +=
			minutes > 0
				? (returnValue !== '' ? ' ' : '') + minutes + minsInLanguage
				: '';
	}
	if (returnValue.indexOf('hours') === -1) {
		seconds = seconds - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60;

		let secondsInLanguage = my8n('secs');

		returnValue +=
			seconds > 0
				? (returnValue !== '' ? ' ' : '') + seconds + secondsInLanguage
				: '';
	}
	return returnValue;
}

export default formatTime;
