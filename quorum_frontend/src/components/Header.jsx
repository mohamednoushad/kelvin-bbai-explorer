import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import '../styles/components/Header.css';

import Card from '../components/Card';
import SearchBar from '../components/SearchBar';

import { my8n } from '../services/localizationService';

class Header extends React.Component {
	render() {
		return (
			<Card className={classNames('header', 'container')}>
				<Link className="header__logo" to="/">
					{my8n('SITE__TITLE')}
				</Link>
				<SearchBar />
			</Card>
		);
	}
}

export default Header;
