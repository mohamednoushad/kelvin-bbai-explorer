import React from 'react';
import { Link } from 'react-router-dom';

import DetailList from '../components/DetailList';

import getMainUrl from '../utils/getMainUrl';
import { backendAPIs } from '../strings/api';

import { my8n } from '../services/localizationService';

function DetailListPage(props) {
	const url = getMainUrl(props);
	let api,
		title,
		id,
		columns = [];

	if (url === 'blocks') {
		api = backendAPIs.LISTBLOCKS;
		title = my8n('Blocks');
		columns = [
			{
				name: my8n('Block_Height'),
				key: 'number'
			},
			{
				name: my8n('Age'),
				key: 'timestamp'
			},
			{
				name: my8n('Hash'),
				key: 'blockHash',
				render: hash => <div>{hash.substr(0, 24) + '... '}</div>
			},
			{
				name: my8n('Total_Transactions'),
				key: 'totalTransactions'
			}
		];
	} else if (url === 'transactions') {
		api = backendAPIs.LISTTRANSACTIONS;
		title = my8n('Transactions');
		columns = [
			{
				name: my8n('Hash'),
				key: 'transactionHash',
				render: hash => <div>{hash.substr(0, 14) + '...'}</div>
			},
			{
				name: my8n('Block'),
				key: 'blockNumber',
				render: blockNumber => (
					<Link to={`/block/${blockNumber}`}>{blockNumber}</Link>
				)
			},
			{
				name: my8n('Age'),
				key: 'timestamp'
			},
			{
				name: my8n('From'),
				key: 'from',
				render: fromAddress => (
					<Link to={`/address/${fromAddress}`}>
						{fromAddress.substr(0, 14) + '...'}
					</Link>
				)
			},
			{
				name: my8n('To'),
				key: 'to',
				render: toAddress => (
					<Link to={`/address/${toAddress}`}>
						{toAddress.substr(0, 14) + '...'}
					</Link>
				)
			}
		];
	} else {
		id = props.match.params.id;
		api = backendAPIs.LISTADDRESS + id;
		title = my8n('Address');
		columns = [
			{
				name: my8n('Txn_Hash'),
				key: 'transactionHash',
				render: transactionHash => (
					<Link to={`/tx/${transactionHash}`}>
						{transactionHash.substr(0, 14) + '...'}
					</Link>
				)
			},
			{
				name: my8n('Block'),
				key: 'blockNumber',
				render: blockNumber => (
					<Link to={`/block/${blockNumber}`}>{blockNumber}</Link>
				)
			},
			{
				name: my8n('Age'),
				key: 'timestamp'
			},
			{
				name: my8n('From'),
				key: 'from',
				render: fromAddress => {
					if (fromAddress === id) {
						return <div>{fromAddress.substr(0, 14) + '...'}</div>;
					} else {
						return (
							<Link to={`/address/${fromAddress}`}>
								{fromAddress.substr(0, 14) + '...'}
							</Link>
						);
					}
				}
			},
			{
				name: my8n('To'),
				key: 'to',
				render: toAddress => {
					if (toAddress === id) {
						return <div>{toAddress.substr(0, 14) + '...'}</div>;
					} else {
						return (
							<Link to={`/address/${toAddress}`}>
								{toAddress.substr(0, 14) + '...'}
							</Link>
						);
					}
				}
			}
		];
	}

	return <DetailList api={api} title={title} columns={columns} />;
}

export default DetailListPage;
