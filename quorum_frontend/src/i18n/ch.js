export default {
	SITE__TITLE: 'BBAI链扫描',

	FOOTER__HEADER: '由BBAI提供支持',
	FOOTER__SUBHEADER:
		'BBAI Chain Scan是BBAI区块链（一种去中心化的智能合约平台）的区块浏览器。',
	FOOTER__COMPANY: '公司',
	FOOTER__COMPANY__LINK__ONE: 'https://link1',
	FOOTER__COMPANY__LINK__TWO: 'https://link1',
	FOOTER__COMPANY__LINK__THREE: 'https://link1',
	FOOTER__SOCIAL: '社会的',
	FOOTER__SOCIAL__LINK__ONE: 'https://link1',
	FOOTER__SOCIAL__LINK__TWO: 'https://link1',
	FOOTER__SOCIAL__LINK__THREE: 'https://link1',

	DETAILLISTHEADER__TX: '显示最近50万笔交易',

	LIST__BUTTON: '查看全部',

	LISTITEM__FROM: '从',
	LISTITEM__TO: '至',
	LISTITEM__BLOCK__TITLE: '块',
	LISTITEM__TX__TITLE: '交易',
	LISTITEM__TXNS: '交易次数',

	DASHBOARD__BLOCK: '最新区块',
	DASHBOARD__TRANSACTIONS: '交易方式',
	DASHBOARD__CONTRACTS: '合约',

	SEARCH__PLACEHOLDER: '按地址/ Txn哈希/块搜索',

	PAGINATION__FIRST: '第一',
	PAGINATION__LAST: '持续',
	PAGINATION__PAGE: '页',
	PAGINATION__PAGE_OF: '的',
	Blocks: '积木',
	Transactions: '交易次数',
	days: ' 天 ',
	hours: ' 小时 ',
	mins: ' 分钟 ',
	secs: ' 秒 ',
	Block: '块',
	Block_Height: '块高',
	Hash: '杂凑',
	Timestamp: '时间戳记',
	Total_Transactions: '总交易',
	Difficulty: '困难',
	Total_Difficulty: '总难度',
	Sha3Uncles: 'Sha3Uncles',
	Transaction: '交易',
	Block_Hash: '区块哈希',
	Number: '数',
	Nonce: '随机数',
	From: '从',
	To: '至',
	Age: '年龄',
	Address: '地址',
	Txn_Hash: 'Txn哈希'
};
