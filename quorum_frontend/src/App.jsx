import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import './App.css';

import DetailViewPage from './container/DetailViewPage';
import DetailListPage from './container/DetailListPage';
import Header from './components/Header';
import Footer from './components/Footer';
import HomePage from './container/HomePage';
import ErrorPage from './components/ErrorPage';

class App extends React.Component {
	changeLanguage = e => {
		window.changeLanguage(e.target.dataset.language);
		this.forceUpdate();
	};

	render() {
		return (
			<BrowserRouter>
				<Header />
				<div className="list__wrapper">
					<Link
						className="list__button"
						data-language="en"
						onClick={this.changeLanguage}
					>
						English
					</Link>
					<Link
						className="list__button"
						data-language="ch"
						onClick={this.changeLanguage}
					>
						Chinese 中文
					</Link>
				</div>
				<div className={classNames('container', 'routerContainer')}>
					<Switch>
						<Route path="/" exact component={HomePage} />
						<Route path="/(block|tx)/:id" exact component={DetailViewPage} />
						<Route path="/address/:id" exact component={DetailListPage} />
						<Route path="/(blocks|transactions)/" component={DetailListPage} />
						<Route path="/error" component={ErrorPage} />
						<Route component={() => <ErrorPage error={true} />} />
					</Switch>
				</div>
				<Footer />
			</BrowserRouter>
		);
	}
}

export default App;
